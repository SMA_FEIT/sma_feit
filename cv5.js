var proj = app.project
var rI = proj.rootItem
var qe = app.enableQE()
var qeProj = qe.project
var file = rI.children
var extension = ""

function organiseProject() {
  var binVideo = rI.createBin("VIDEOFolder")
  var binAudio = rI.createBin("AUDIOFolder")
  var binImage = rI.createBin("IMAGEFolder")

  var arrayVideo = []
  var arrayAudio = []
  var arrayImage = []

  for (var i = 0; i < rI.children.numItems; i++) {
    var object = rI.children[i]
    var name = rI.children[i].name
    var extension = name.substring(name.length - 3, name.length).toLowerCase()
    if (extension == "jpg" || extension == "png") {
      arrayImage.push(object)
    }
    if (extension == "mp3" || extension == "wav") {
      arrayAudio.push(object)
    }
    if (extension == "mp4" || extension == "mts") {
      arrayVideo.push(object)
    }
  }
  moveToFolder(arrayAudio, binAudio)
  moveToFolder(arrayImage, binImage)
  moveToFolder(arrayVideo, binVideo)
}

function moveToFolder(items, folder) {
  for (var i = 0; i < items.length; i++) {
    items[i].moveBin(folder)
  }
}

organiseProject()
