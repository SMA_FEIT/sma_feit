var seq1 = proj.activeSequence
var audioTrack1 = seq1.audioTracks[1]
var clip1 = audioTrack1.clips[0]
var proj = app.project
var rI = proj.rootItem
var qe = app.enableQE()
var qeProj = qe.project
var seq1_QE = qe.project.getActiveSequence(0)
var videoTrack1_QE = seq1_QE.getVideoTrackAt(0)

var numClipsInTrack = seq1_QE.getVideoTrackAt(0).numItems
$.writeln("Zoznam videoTrackov v aktívnej sekvencií: ")
$.writeln(numClipsInTrack - 1)
for (var i = 0; i < numClipsInTrack - 1; i++) {
  var clip = seq1_QE.getVideoTrackAt(0).getItemAt(i).name
  $.writeln(clip)
}
$.writeln("--------------------------")

var seq1_QE = qe.project.getActiveSequence(0)
var audioTrack1_QE = seq1_QE.getAudioTrackAt(1)
var clip1_QE = audioTrack1_QE.getItemAt(0)
clip1_QE.addAudioEffect(qe.project.getAudioEffectByName("Pitch Shifter"))
var components = clip1.components
for (var j = 0; j < components.numItems; j++) {
  if (components[j].displayName == "Pitch Shifter") {
    pitchshifter = components[j]
  }
}
var pitchshifterEffect = pitchshifter.properties[1]
pitchshifterEffect.setValue(-10)
var duration = clip1.end.seconds
$.writeln(duration)
