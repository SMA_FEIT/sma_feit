var proj = app.project
var rootItem = proj.rootItem
var qe = app.enableQE()
var qeProj = qe.project
var pocet = qeProj.numItems
$.writeln("Zoznam súborov s projekte: ")
$.writeln(pocet)
$.writeln("--------------------------")
for (var i = 0; i < pocet; i++) {
  $.writeln(qeProj.getItemAt(i).name)
}
$.writeln("--------------------------")
var pocetBinov = qeProj.numBins
$.writeln("Zoznam priečinkov s projekte: ")
$.writeln(pocetBinov)
$.writeln("--------------------------")
for (var i = 0; i < pocetBinov; i++) {
  $.writeln(qeProj.getBinAt(i).name)
}
$.writeln("--------------------------")
