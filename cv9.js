var qe = app.enableQE()
var qeProj = qe.project

function makeDecision(decision) {
  if (decision == "undo") {
    qeProj.undo()
  } else if (decision == "redo") {
    qeProj.redo()
  } else if (decision == "cache") {
    qeProj.flushCache()
  } else {
    $.writeln("Zvolte: undo, redo alebo cache")
  }
}

makeDecision("redo")
