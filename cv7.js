var proj = app.project
var rootItem = proj.rootItem
var qe = app.enableQE()
var qeProj = qe.project
var seq1 = proj.activeSequence
var videoTrack1 = seq1.videoTracks[0]
var clip1 = videoTrack1.clips[0]
var clip2 = videoTrack1.clips[1]
var clip3 = videoTrack1.clips[2]
//var seq1 = proj.createNewSequence("Seq1", "Seq1")
var seq1_QE = qe.project.getActiveSequence(0)
var videoTrack1_QE = seq1_QE.getVideoTrackAt(0)
var clip1_QE = videoTrack1_QE.getItemAt(0)
var clip2_QE = videoTrack1_QE.getItemAt(1)
var clip3_QE = videoTrack1_QE.getItemAt(2)
clip1_QE.addVideoEffect(qe.project.getVideoEffectByName("Compound Blur")) //nazov efektu
clip2_QE.addVideoEffect(qe.project.getVideoEffectByName("Camera Blur")) //nazov efektu
clip3_QE.addVideoEffect(qe.project.getVideoEffectByName("Black & White")) //nazov efektu
var components = clip1.components
var CDTransisiton = qe.project.getVideoTransitionByName("Cross Dissolve")
var item = videoTrack1_QE.getItemAt(0)
item.addTransition(CDTransisiton, false, "00;00;3;00")
var CDTransisiton1 = qe.project.getVideoTransitionByName("Cross Dissolve")
var item = videoTrack1_QE.getItemAt(1)
item.addTransition(CDTransisiton1, false, "00;00;1;00")
