var qe = app.enableQE()
var qeProj = qe.project

function getLists(typZoznamu) {
  if (typZoznamu == "AudioEffect") {
    $.writeln("AudioEffect List: ")
    $.writeln(qeProj.getAudioEffectList())
    $.writeln("-----------------")
  } else if (typZoznamu == "VideoEffect") {
    $.writeln("VideoEffect List: ")
    $.writeln(qeProj.getVideoEffectList())
    $.writeln("-----------------")
  } else if (typZoznamu == "VideoTransition") {
    $.writeln("VideoTransition List: ")
    $.writeln(qeProj.getVideoTransitionList())
    $.writeln("-----------------")
  } else if (typZoznamu == "AudioTransition") {
    $.writeln("VideoTransition List: ")
    $.writeln(qeProj.getAudioTransitionList())
    $.writeln("-----------------")
  } else {
    $.writeln(
      "Môžete zadať: AudioEffect, VideoEffect, VideoTransition, AudioTransition"
    )
  }
}

getLists("AudioEffect")
getLists("VideoEffect")
getLists("VideoTransition")
getLists("AudioTransition")
